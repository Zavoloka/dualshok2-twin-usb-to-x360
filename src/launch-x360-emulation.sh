#!/bin/bash
DEFAULT_TEXT_COLOR="\e[0m"
X360_MAPPER=""
# process args
while getopts e:c: opts; do
   case ${opts} in
      e) EVENT=${OPTARG} ;;
      c) X360_MAPPER=${OPTARG} ;;
   esac
done

echo -e "#Selected event key : "${EVENT/ /}


# get params from mapper (*.cfg)
KEY_MAPPER_DATA=$(cat "$X360_MAPPER")

# check if xboxdrv is installed
# http://stackoverflow.com/questions/1251999/how-can-i-replace-a-newline-n-using-sed
KEY_MAPPER_DATA=$(echo $KEY_MAPPER_DATA | sed -e 's/\\//g' -e 's/, /,/g')
echo -e "\e[0m#Key scheme : \e[93m"$KEY_MAPPER_DATA

echo -e "\n\e[34m\n"
sudo xboxdrv --evdev /dev/input/$EVENT $KEY_MAPPER_DATA
