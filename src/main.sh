#!/bin/bash
# baf-baf

# List of possible arguments :
#> -d %device_name% - device name to search
#> -t - test device input via 'evtest'
#> -m %config_path% - use mapper config to map all buttons into x360 format

# TODO : add php7 and py3 parsers + add code to check if
# corresponding interpreters exist
# e.g. -p php || py || grep (by default)
# TODO : refactoring (with functions)
# TODO : generate doc
# TODO : readme file
# TODO : test with video


HELP_INFO="hey ho"

# default key mapper
CONFIG_MAPPER_PATH="src/config/x360-key-mapper.cfg"

# process args
TEST_EVENT=false
DEBUG_MODE=false
while getopts n:dthc: opts; do
   case ${opts} in
      n) DEVICE_NAME=${OPTARG} ;;
      d) DEBUG_MODE=true ;;
      t) TEST_EVENT=true ;;
      c) CONFIG_MAPPER_PATH=${OPTARG} ;;
      h) echo -e $HELP_INFO
         exit ;;
   esac
done


# handle debug mode which prints all commands to terminal
if [ "$DEBUG_MODE" = true ]
then
  set -x
fi


#  trim DEVICE_NAME and check if it empty
# output colors can be found by the following link  (http://misc.flogisoft.com/bash/tip_colors_and_formatting)
if [ -z  "${DEVICE_NAME/ /}" ]
  then
    echo -e "\e[31mDevice name is empty. Please use key -n to set it."
    exit
fi

# regex which searches through /proc/bus/input/devices
echo "Device to search : "$DEVICE_NAME
GREP_REGEX="(I:)((.)+\n)+(.)+((?i)(${DEVICE_NAME}))((.)+\n)+(.)+"


# get all connected devices by key name (grep uses PERL RegEx handler)
#> -P -> --perl-regexp,
#> -o -> --only-matching
#> -z -> --null-data
#>    Treat  the  input  as  a set of lines, each terminated by a zero
#>    byte (the ASCII NUL character) instead of a newline.   Like  the
#>    -Z  or --null option, this option can be used with commands like
#>    sort -z to process arbitrary file names.

# save grep output to the variale and print it
GREP_OUTPUT=$(grep -Pzo "$GREP_REGEX"  /proc/bus/input/devices)
echo -e "Search result : \n\e[96m"$GREP_OUTPUT


# TODO : Check if event is empty, or it contains multiple events
# TODO : process each element in array

# search for event IDs and save them into array, then count
EVENT_IDS=($(echo $GREP_OUTPUT | grep -Po "event\d+"))
COUNT_OF_EVENT_IDS=${#EVENT_IDS[@]}
SELECTED_EVENT_ID=0

# debug
#COUNT_OF_EVENT_IDS=2
echo -e "\e[0mAvailable devices : "$COUNT_OF_EVENT_IDS



case ${COUNT_OF_EVENT_IDS} in
   0) exit ;;
   1) SELECTED_EVENT_ID=0 ;;
   # TODO : add error handler/validation here
   2) echo -e "Which device you want to use (number)?"
      read USER_EVENT_INDEX
      SELECTED_EVENT_ID=$((${USER_EVENT_INDEX}-1))
    ;;
esac

EVENT=${EVENT_IDS[$SELECTED_EVENT_ID]}

# evtest part
if [ "$TEST_EVENT" = true ]
then
  #evtest doc http://manpages.ubuntu.com/manpages/zesty/man1/evtest.1.html
  echo -e "\e[36mCyanLaunch evtest for event "$EVENT
  sudo evtest --grab /dev/input/$EVENT
fi

echo -e "#Launch mapper"
/bin/bash ./src/launch-x360-emulation.sh -e $EVENT -c $CONFIG_MAPPER_PATH



# parse grep result
# check event sudo evtest --grab /dev/input/event8 if key is specified

# Bucket for commands and tips:
# -------------------------------
# cat /proc/bus/input/devices
# grep "(\n){0}I:((?!I:)|(\s|\S))+(?i)(chicony)" /proc/bus/input/devices
# php pattern (I:)((?!I:)(.|\n))+((?i)chic)((.)+\n)+(.)+
# test Chicony USB Keyboard
# sudo evtest --grab /dev/input/event8
# grep -P  "(I:)((?I:){0}(.|\n))+((?i)chic)((.)+\n)+(.)+" /proc/bus/input/devices
# (I:)((?!I:)(.|\n))+(chic)((.)+\n)+(.)+ php
# (I:)((.)+\n)+(.)+((?i)chic)((.)+\n)+(.)+ CORRECT PHP without exlude
# grep -Pzo  "(I:)((.)+\n)+(.)+((?i)+\n)+(.)+" /proc/bus/input/devices
