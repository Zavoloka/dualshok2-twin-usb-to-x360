#!/bin/bash

# This script uses docco  http://ashkenas.com/docco/

# Use the following command to install docco
#> sudo npm install -g docco

# generate html documentation
#find src/ -name '*.sh' | xargs docco -o "doc"
docco -o "doc" ./src/*.sh
